https://gitlab.com/miage.19/home/wikis/home

TP1 --wildfly
pour lancer wildfly il faut lancer standalone.bat sur C:/opt/wildfly/bin
url d'admin localhost:9990/console
url appli localhost:8080/filestore-arnaud-1.0.0-SNAPSHOT/hello
			(nom du war)
url api http://localhost:8080/filestore-arnaud-1.0.0-SNAPSHOT/api/hello

dans le pom on met un war en sorti au lieu du pom ou jar

TP2 --thorntail
pour thorntail
lancer java -jar .\target\filestore-arnaud-1.0.0-SNAPSHOT-thorntail.jar
et url http://localhost:8180/hello


diff entre wildfly et thorntail
avec throntail on fait un gros jar qui va pouvoir etre mis plus facilement dans un cloud par exemple
wildfly c'est un war qui est juste l'application


TP3 --couche API
api pour les fichiers ==> passage vers couche métier avec un EJB (exemple sécurité du fichier, date, format)

http://localhost:8180/status?username=michmich

POST http://localhost:8180/api/files fichier en json

{
    "name": "alain.txt",
    "size": 0,
    "modificationDate": 9999999999999,
    "content": "bernard",
    "data": "file:///C:/Users/User/.filestore/alain.txt"
}

POST http://localhost:8180/api/files
name + data (fichier)
accept:multipart/form-data


TP4 --test et validation

POST http://localhost:8180/api/files
{
	"name":"truc/~.txt",
	"size":0,
	"content":"machin"
}

TP6 --dokku

ssh dokku@filestore apps:list
git push dokku master

TP7 --JPA
utilise des ids au lieu du path lors du get ou du add car c'est plus pratique en web de partager avec juste l'id qu'avec le path