<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FileStore</title>

    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Filestore</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/api/files">Home <span class="sr-only">(current)</span></a>
                <a class="nav-link" href="/api/status">Status</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">
    <div class="homepage">
        <div class="files">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Size</th>
                    <th scope="col">Type</th>
                    <th scope="col">LastModification</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <#list content.values as item>
                    <tr>
                        <th scope="row">${item?index}</th>
                        <td>${item.name}</td>
                        <td>${item.size}</td>
                        <td>${item.mimeType}</td>
                        <td>${item.modificationDate?number_to_datetime}</td>
                        <td><a href="/api/files/${item.name}">download</a></td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
        <div class="upload">
            <h3>Upload New File</h3>
            <form method="post" action="/api/files" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="filename">Filename</label>
                        <input type="text" class="form-control" id="filename" name="name" placeholder="filename">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="file">File</label>
                        <input type="file" class="form-control-file" id="file" name="data">
                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary mb-2">Upload</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>