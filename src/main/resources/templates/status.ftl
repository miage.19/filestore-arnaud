<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FileStore Status page</title>

    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Filestore</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/api/files">Home</a>
                <a class="nav-link" href="/api/status">Status <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">
    <div class="homepage">
        <div class="page-header">
            <h1>FilesStore Status</h1>
        </div>
        <div class="status">
            <h1>Server Status</h1>
            <div><span>Uptime: </span><span>${content.server.uptime}</span></div>
            <div><span>Nb CPUs: </span><span>${content.server.nbCpus}</span></div>
            <div><span>Total Memory: </span><span>${content.server.totalMemory}</span></div>
            <div><span>Available Memory: </span><span>${content.server.availableMemory}</span></div>
            <div><span>Max Memory: </span><span>${content.server.maxMemory}</span></div>

            <#list content.stores as store>
                <h2>Store n°${store?index}</h2>
                <div><span>Name: </span><span>${store.name}</span></div>
                <div><span>Total Space: </span><span>${store.totalSpace}</span></div>
                <div><span>Free Space: </span><span>${store.usableSpace}</span></div>
                <div><span>Read Only: </span><span>${store.readOnly?string('yes', 'no')}</span></div>
                <div><span>Downloads: </span><span>${store.downloads}</span></div>
                <div><span>Uploads: </span><span>${store.uploads}</span></div>
            </#list>
        </div>
    </div>
</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>