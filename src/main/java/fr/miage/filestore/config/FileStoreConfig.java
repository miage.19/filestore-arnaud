package fr.miage.filestore.config;

import java.nio.file.Path;

public interface FileStoreConfig {

    Path getHome();

}
