package fr.miage.filestore.config;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Named;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named("config")
@Alternative
@ApplicationScoped
public class TestFileStoreConfig implements  FileStoreConfig {

    private static final Logger LOGGER = Logger.getLogger(TestFileStoreConfig.class.getName());

    private Path home;

    @PostConstruct
    public void init() {
        LOGGER.log(Level.INFO, "Initialising config (TEST)");
        home = Paths.get("/tmp", String.valueOf(System.currentTimeMillis()));
        LOGGER.log(Level.INFO, "Filestore home set to : " + home.toString());
    }

    @Override
    public Path getHome() {
        return home;
    }
}
