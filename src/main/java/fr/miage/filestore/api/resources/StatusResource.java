package fr.miage.filestore.api.resources;

import fr.miage.filestore.api.dto.FileStoreStatus;
import fr.miage.filestore.api.writer.Template;
import fr.miage.filestore.api.writer.TemplateContent;
import fr.miage.filestore.files.FileServiceMetrics;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Paths;

@Path("status")
public class StatusResource {

    private static final java.nio.file.Path store = Paths.get(System.getProperty("user.home"), ".filestore");

    @EJB
    private FileServiceMetrics metrics;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public FileStoreStatus getStatusJson() throws IOException {
        return internalGetStatus();
    }

    @GET
    @Template(name = "status")
    @Produces(MediaType.TEXT_HTML)
    public TemplateContent getStatusHtml() throws IOException {
        TemplateContent<FileStoreStatus> content = new TemplateContent();
        content.setContent(internalGetStatus());
        return content;
    }

    private FileStoreStatus internalGetStatus() throws IOException {
        FileStoreStatus status = new FileStoreStatus();
        FileStoreStatus.Server serverStatus = new FileStoreStatus.Server();
        serverStatus.setNbCpus(Runtime.getRuntime().availableProcessors());
        serverStatus.setTotalMemory(Runtime.getRuntime().totalMemory());
        serverStatus.setAvailableMemory(Runtime.getRuntime().freeMemory());
        serverStatus.setMaxMemory(Runtime.getRuntime().maxMemory());
        serverStatus.setUptime(ManagementFactory.getRuntimeMXBean().getUptime());
        status.setServer(serverStatus);

        FileStore filestore = Files.getFileStore(store);
        FileStoreStatus.Store storeStatus = new FileStoreStatus.Store();
        storeStatus.setName(filestore.name());
        storeStatus.setReadOnly(filestore.isReadOnly());
        storeStatus.setTotalSpace(filestore.getTotalSpace());
        storeStatus.setUsableSpace(filestore.getUsableSpace());
        storeStatus.setUploads(metrics.getNbUploads());
        storeStatus.setDownloads(metrics.getNbDownloads());
        status.getStores().add(storeStatus);

        return status;
    }
}
