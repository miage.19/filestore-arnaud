package fr.miage.filestore.api.resources;

import fr.miage.filestore.files.FileService;
import fr.miage.filestore.files.entity.FileItem;
import fr.miage.filestore.files.exceptions.FileItemNotFoundException;
import fr.miage.filestore.files.exceptions.FileServiceException;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("files")
public class FilesResource {

    private static final Logger LOGGER = Logger.getLogger(FilesResource.class.getName());

    @EJB
    private FileService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response root() throws FileItemNotFoundException, FileServiceException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files");
        //TODO Load the root and get its content as a FileItem list
        FileItem item = service.get(null);
        return Response.ok(item).build();
    }

    /*
    @GET
    @Template(name = "files")
    @Produces(MediaType.TEXT_HTML)
    public TemplateContent<ItemCollection<FileItem>> listCollectionHtml(
            @QueryParam("limit") @DefaultValue("10") int limit,
            @QueryParam("offset") @DefaultValue("0") int offset) {
        LOGGER.log(Level.INFO, "GET /api/files (html)");

        FileFilter filter = new FileFilter();
        filter.setLimit(limit);
        filter.setOffset(offset);

        ItemCollection<FileItem> items = new ItemCollection<>();
        items.setSize(service.countFiles(filter));
        items.setLimit(limit);
        items.setOffset(offset);
        items.setValues(service.listFiles(filter));

        TemplateContent<ItemCollection<FileItem>> content = new TemplateContent<>();
        content.setContent(items);
        return content;
    }

    @Produces(MediaType.APPLICATION_JSON)
    public ItemCollection<FileItem> listCollection(
            @QueryParam("limit") @DefaultValue("10") int limit,
            @QueryParam("offset") @DefaultValue("0") int offset) throws FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files");
        FileFilter filter = new FileFilter();
        filter.setLimit(limit);
        filter.setOffset(offset);

        ItemCollection<FileItem> items = new ItemCollection<>();
        items.setSize(service.countFiles(filter));
        items.setLimit(limit);
        items.setOffset(offset);
        items.setValues(service.listFiles(filter));

        return items;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addFileJson(@Valid FileItem item, @Context UriInfo info) throws FileAlreadyExistsException, FileServiceException {
        LOGGER.log(Level.INFO, "POST /api/files (json)");
        FileItem createdItem = service.addFile(item.getName(), new ByteArrayInputStream(item.getContent().getBytes()));
        URI createdUri = info.getRequestUriBuilder().path(createdItem.getName()).build();
        return Response.created(createdUri).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response addFileForm(@MultipartForm @Valid FileUploadForm item, @Context UriInfo info) throws FileAlreadyExistsException, FileServiceException {
        LOGGER.log(Level.INFO, "POST /api/files (multipart)");
        FileItem createdItem = service.addFile(item.getName(), item.getData());
        URI createdUri = info.getRequestUriBuilder().path(createdItem.getName()).build();
        return Response.created(createdUri).build();
    }

    @GET
    @Path("{name}")
    @Produces(MediaType.MEDIA_TYPE_WILDCARD)
    public Response getFile(@PathParam("name") String name) throws FileNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files/" + name);
        FileItem item = service.getFile(name);
        return Response.ok(service.getFileContent(name))
                .header("Content-Type", item.getMimeType())
                .header("Content-Length", item.getSize())
                .header("Content-Disposition", "attachment; filename=" + item.getName()).build();
    }

    @PUT
    @Path("{name}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFileJson(@PathParam("name") String name, @Valid FileItem item) throws FileNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "PUT /api/files/" + name + "(json)");
        throw new FileServiceException("NOT IMPLEMENTED");
    }

    @PUT
    @Path("{name}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateFileForm(@PathParam("name") @Filename String name, @MultipartForm @Valid FileUploadForm item) throws FileNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "PUT /api/files/" + name + "(multipart)");
        throw new FileServiceException("NOT IMPLEMENTED");
    }


    @DELETE
    @Path("{name}")
    public Response deleteFile(@PathParam("name") @Filename String name) throws FileServiceException, FileNotFoundException {
        LOGGER.log(Level.INFO, "DELETE /api/files/" + name);
        service.deleteFile(name);
        return Response.noContent().build();
    }*/

}

