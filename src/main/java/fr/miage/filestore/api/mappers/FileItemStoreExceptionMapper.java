package fr.miage.filestore.api.mappers;

import fr.miage.filestore.files.exceptions.FileServiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class FileItemStoreExceptionMapper implements ExceptionMapper<FileServiceException> {

    @Override
    public Response toResponse(FileServiceException e) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
    }
}
