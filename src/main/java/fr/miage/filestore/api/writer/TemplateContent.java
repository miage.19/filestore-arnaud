package fr.miage.filestore.api.writer;

public class TemplateContent<T> {

    private T content;

    public TemplateContent() {
    }

    public TemplateContent(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
