package fr.miage.filestore.api.dto;

public class ApiError {

    private long id;
    private String message;
    private String localizedMessage;
    private StackTraceElement[] stacktrace;

    private ApiError(Builder builder) {
        this.setId(System.currentTimeMillis());
        this.setMessage(builder.exception.getMessage());
        this.setLocalizedMessage(builder.exception.getMessage());
        this.setStacktrace(builder.exception.getStackTrace());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(String localizedMessage) {
        this.localizedMessage = localizedMessage;
    }

    public StackTraceElement[] getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(StackTraceElement[] stacktrace) {
        this.stacktrace = stacktrace;
    }

    public static class Builder {

        private Exception exception;

        public Builder() {
        }

        public Builder exception(Exception e) {
            this.exception = e;
            return this;
        }

        public ApiError build() {
            return new ApiError(this);
        }

    }

}
