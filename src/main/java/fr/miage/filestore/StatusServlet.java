package fr.miage.filestore;

import com.sun.net.httpserver.HttpServer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Status", urlPatterns = {"/status"})
public class StatusServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("foo", (request.getParameterMap().containsKey("username"))?request.getParameter("username"):"toto");
        request.getRequestDispatcher("jsp/status.jsp").forward(request, response);
    }
}
