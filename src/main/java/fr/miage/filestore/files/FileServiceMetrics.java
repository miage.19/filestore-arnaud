package fr.miage.filestore.files;

public interface FileServiceMetrics {

    int getNbUploads();

    int getNbDownloads();

}

