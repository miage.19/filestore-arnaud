package fr.miage.filestore.files;

import fr.miage.filestore.config.FileStoreConfig;
import fr.miage.filestore.files.entity.FileItem;
import fr.miage.filestore.files.exceptions.FileItemNotEmptyException;
import fr.miage.filestore.files.exceptions.FileItemNotFoundException;
import fr.miage.filestore.files.exceptions.FileServiceException;
import fr.miage.filestore.store.BinaryStoreService;
import fr.miage.filestore.store.exceptions.BinaryStoreServiceException;
import fr.miage.filestore.store.exceptions.BinaryStreamNotFoundException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@Interceptors({FileServiceMetricsBean.class})
public class FileServiceBean implements FileService {

    private static final Logger LOGGER = Logger.getLogger(FileService.class.getName());

    @EJB
    private BinaryStoreService store;

    @Inject
    private FileStoreConfig config;

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;

    @PostConstruct
    private void init() {
        boolean bootstrap = false;
        try {
            loadItem(FileItem.ROOT_ID);
        } catch (FileItemNotFoundException e ) {
            bootstrap = true;
        }
        synchronized (this) {
            if ( bootstrap ) {
                LOGGER.log(Level.INFO, "Root item does not exists, bootstraping");
                FileItem item = new FileItem();
                item.setName("root");
                item.setId(FileItem.ROOT_ID);
                item.setMimeType(FileItem.FOLDER_MIME_TYPE);
                em.persist(item);
                LOGGER.log(Level.INFO, "Bootstrap done, root item exists now.");
            }
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<FileItem> list(String id) throws FileServiceException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "List children of item: " + id);
        FileItem item = loadItem(id);
        if ( !item.isFolder() ) {
            throw new FileServiceException("Item is not a folder, unable to list children");
        }
        TypedQuery<FileItem> query = em.createNamedQuery("FileItem.listChildren", FileItem.class).setParameter("parent", item.getId());
        List<FileItem> items = query.getResultList();
        items.sort(new FileItem.NameComparatorAsc());
        return items;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileItem add(String id, String name) throws FileServiceException, FileAlreadyExistsException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "Adding children to item with id: " + id);
        FileItem parent = loadItem(id);
        if ( !parent.isFolder() ) {
            throw new FileServiceException("Item is not a folder, unable to add children");
        }
        TypedQuery<Long> query = em.createNamedQuery("FileItem.countChildrenForName", Long.class).setParameter("parent", parent.getId()).setParameter("name", name);
        if ( query.getSingleResult() > 0 ) {
            throw new FileAlreadyExistsException("An children with name: " + name + " already exists in item with id: " + id);
        }
        FileItem child = new FileItem();
        child.setId(UUID.randomUUID().toString());
        child.setParent(parent.getId());
        child.setName(name);
        child.setMimeType(FileItem.FOLDER_MIME_TYPE);
        em.persist(child);
        parent.setModificationDate(new Date());
        parent.setSize(parent.getSize() + 1);
        em.persist(parent);
        return child;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileItem add(String id, String name, InputStream stream) throws FileServiceException, FileAlreadyExistsException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "Adding children to item with id: " + id);
        try {
            FileItem parent = loadItem(id);
            if ( !parent.isFolder() ) {
                throw new FileServiceException("Item is not a folder, unable to add children");
            }
            TypedQuery<Long> query = em.createNamedQuery("FileItem.countChildrenForName", Long.class).setParameter("parent", parent.getId()).setParameter("name", name);
            if ( query.getSingleResult() > 0 ) {
                throw new FileAlreadyExistsException("An children with name: " + name + " already exists in item with id: " + id);
            }
            String contentId = store.put(stream);
            stream.close();
            long size = store.size(contentId);
            String type = store.type(contentId, name);
            FileItem child = new FileItem();
            child.setId(UUID.randomUUID().toString());
            child.setParent(parent.getId());
            child.setName(name);
            child.setMimeType(type);
            child.setSize(size);
            child.setContentId(contentId);
            em.persist(child);
            parent.setModificationDate(new Date());
            parent.setSize(parent.getSize() + 1);
            em.persist(parent);
            return child;
        } catch (BinaryStoreServiceException | BinaryStreamNotFoundException | IOException e ) {
            throw new FileServiceException("Unable to find binary stream for item", e);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public FileItem get(String id) throws FileServiceException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "Getting item with id: " + id);
        return loadItem(id);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public InputStream getContent(String id) throws FileServiceException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "Getting item content for id: " + id);
        try {
            FileItem item = loadItem(id);
            return store.get(item.getContentId());
        } catch (BinaryStoreServiceException | BinaryStreamNotFoundException e ) {
            throw new FileServiceException("Unable to find binary stream for item", e);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void remove(String id, String name) throws FileServiceException, FileItemNotFoundException, FileItemNotEmptyException {
        LOGGER.log(Level.INFO, "Removing from item with id: " + id + ", the children with name: " + name);
        try {
            FileItem parent = loadItem(id);
            if ( !parent.isFolder() ) {
                throw new FileServiceException("Item is not a folder, unable to remove children");
            }
            TypedQuery<FileItem> query = em.createNamedQuery("FileItem.findChildrenForName", FileItem.class).setParameter("parent", parent.getId()).setParameter("name", name).setMaxResults(1);
            FileItem children;
            try {
                children = query.getSingleResult();
            } catch (NoResultException e) {
                throw new FileItemNotFoundException("No children found with name: " + name + " in parent item with id: " + id);
            }
            if ( children.isFolder() ) {
                Long folderSize = em.createNamedQuery("FileItem.countChildren", Long.class).setParameter("parent", children.getId()).getSingleResult();
                if ( folderSize > 0 ) {
                    throw new FileItemNotEmptyException("Children is a folder and is not empty, unable to remove, purge before");
                }
            }
            parent.setModificationDate(new Date());
            em.persist(parent);
            if ( !children.isFolder() ) {
                store.delete(children.getContentId());
            }
            parent.setSize(parent.getSize() - 1);
            em.remove(children);
        } catch (BinaryStoreServiceException | BinaryStreamNotFoundException e ) {
            throw new FileServiceException("Unable to find binary stream for item", e);
        }
    }


    //INTERNAL OPERATIONS

    private FileItem loadItem(String id) throws FileItemNotFoundException {
        if ( id == null || id.isEmpty() ) {
            id = FileItem.ROOT_ID;
        }
        FileItem item = em.find(FileItem.class, id);
        if ( item == null ) {
            throw new FileItemNotFoundException("unable to find a item with id: " + id);
        }
        return item;
    }

}
