package fr.miage.filestore.files;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton
public class FileServiceMetricsBean implements FileServiceMetrics {

    private static final Logger LOGGER = Logger.getLogger(FileServiceMetrics.class.getName());

    private static int uploads = 0;
    private static int downloads = 0;

    @Resource(lookup = "java:jboss/ee/concurrency/scheduler/default")
    private ManagedScheduledExecutorService executorService;

    @PostConstruct
    private void init() {
        LOGGER.log(Level.INFO, "Initializing FileServiceMetrics");
        FileServiceMetricsWorker worker = new FileServiceMetricsWorker(this);
        executorService.scheduleAtFixedRate(worker,1, 2, TimeUnit.MINUTES);
    }

    @Override
    public int getNbUploads() {
        return uploads;
    }

    @Override
    public int getNbDownloads() {
        return downloads;
    }

    @AroundInvoke
    public Object intercept(InvocationContext ic) throws Exception {
        LOGGER.entering(ic.getTarget().toString(), ic.getMethod().getName());
        try {
            Object obj =  ic.proceed();
            if ( ic.getMethod().isAnnotationPresent(Metric.class) ) {
                LOGGER.log(Level.INFO, "We have a metric to apply!");
                switch ( ic.getMethod().getAnnotation(Metric.class).type() ) {
                    case UPLOAD: uploads++; break;
                    case DOWNLOAD: downloads++; break;
                }
            }
            return obj;
        } finally {
            LOGGER.exiting(ic.getTarget().toString(), ic.getMethod().getName());
        }
    }

    @Schedule(hour = "*/2", persistent = false) //hour = "*", minute = "*/2"
    private void razLatestMetrics() {
        LOGGER.log(Level.INFO, "reset latests metrics");
        if ( uploads + downloads > 10 ) {
            LOGGER.log(Level.WARNING, "!!HIGH SERVICE USAGE DETECTED!!");
        }
        uploads = 0;
        downloads = 0;
    }

}
