package fr.miage.filestore.files;

import fr.miage.filestore.files.entity.FileItem;
import fr.miage.filestore.files.exceptions.FileItemAlreadyExistsException;
import fr.miage.filestore.files.exceptions.FileItemNotEmptyException;
import fr.miage.filestore.files.exceptions.FileItemNotFoundException;
import fr.miage.filestore.files.exceptions.FileServiceException;

import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.util.List;

public interface FileService {

    List<FileItem> list(String id) throws FileServiceException, FileItemNotFoundException;

    FileItem add(String id, String name) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException, FileAlreadyExistsException;

    FileItem add(String id, String name, InputStream stream) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException, FileAlreadyExistsException;

    FileItem get(String id) throws FileServiceException, FileItemNotFoundException;

    InputStream getContent(String id) throws FileServiceException, FileItemNotFoundException;

    void remove(String id, String name) throws FileServiceException, FileItemNotFoundException, FileItemNotEmptyException;


}
