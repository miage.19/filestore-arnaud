package fr.miage.filestore.files;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FileServiceMetricsWorker implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(FileServiceMetricsWorker.class.getName());

    private FileServiceMetrics metrics;

    public FileServiceMetricsWorker(FileServiceMetrics metrics) {
        LOGGER.log(Level.INFO, "Instanciate FileServiceMetricsWorker");
        this.metrics = metrics;
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "FileServiceMetricsWorker is running");
        LOGGER.log(Level.INFO, "Nb downloads: " +  metrics.getNbDownloads());
        LOGGER.log(Level.INFO, "FileServiceMetricsWorker has worked");
    }
}
