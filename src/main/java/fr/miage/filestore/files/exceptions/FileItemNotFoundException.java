package fr.miage.filestore.files.exceptions;

public class FileItemNotFoundException extends Exception {

    public FileItemNotFoundException(String message) {
        super(message);
    }
}
