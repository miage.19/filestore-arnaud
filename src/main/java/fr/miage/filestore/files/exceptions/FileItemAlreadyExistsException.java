package fr.miage.filestore.files.exceptions;

public class FileItemAlreadyExistsException extends Exception {

    public FileItemAlreadyExistsException(String message) {
        super(message);
    }
}