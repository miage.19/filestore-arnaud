package fr.miage.filestore.files.exceptions;

public class FileItemNotEmptyException extends Exception {
    public FileItemNotEmptyException(String message) {
        super(message);
    }
}
