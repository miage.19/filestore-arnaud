package fr.miage.filestore.store;

import fr.miage.filestore.store.exceptions.BinaryStoreServiceException;
import fr.miage.filestore.store.exceptions.BinaryStreamNotFoundException;

import java.io.InputStream;

public interface BinaryStoreService {

    boolean exists(String key) throws BinaryStoreServiceException;

    long size(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    String type(String key, String name) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    String put(InputStream is) throws BinaryStoreServiceException;

    InputStream get(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    void delete(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

}

